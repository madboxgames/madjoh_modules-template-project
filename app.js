requirejs.config({
    baseUrl: 'lib',
    paths: {
        app				: '../app',
        settings 		: '../app/script/settings',
        madjoh_modules 	: '../madjoh_modules'
    }
});

requirejs(['app/script/main']);